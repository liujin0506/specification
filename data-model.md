## 数据模型

### 放置位置

所有的数据模型文件，都 **必须** 存放在：``app/Models/`` 文件夹中。

命名空间：

```php
namespace App\Models;
```

### User.php

Laravel 5.1 默认安装会把 ``User`` 模型存放在 ``app/User.php``，必须 移动到 ``app/Models`` 文件夹中，并修改命名空间声明为 ``App/Models``，同上。

为了不破坏原有的逻辑点，必须 全局搜索 ``App/User`` 并替换为 ``App/Models/User``。


### 使用基类

所有的 ``Eloquent`` 数据模型 都 **必须** 继承统一的基类 ``App/Models/Model``，此基类存放位置为 ``/app/Models/Model.php``，内容参考以下：

```php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as EloquentModel;

class Model extends EloquentModel
{
    public function scopeRecent($query)
    {
        return $query->orderBy('created_at', 'desc');
    }
}
```

以 Photo 数据模型作为例子继承 Model 基类：

```php
<?php

namespace App\Models;

class Photo extends Model
{
    protected $fillable = ['id', 'user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
```


### 命名规范

数据模型相关的命名规范：

 - 数据模型类名 **必须** 为「单数」, 如：App\Models\Photo
 - 类文件名 **必须** 为「单数」，如：app/Models/Photo.php
 - 数据库表名字 **必须** 为「复数」，多个单词情况下使用[「Snake Case」](https://en.wikipedia.org/wiki/Snake_case) 如：photos, my_photos
 - 数据库表迁移名字 **必须** 为「复数」，如：2014_08_08_234417_create_photos_table.php
 - 数据填充文件名 **必须** 为「复数」，如：PhotosTableSeeder.php
 - 数据库字段名 **必须** 为[「Snake Case」](https://en.wikipedia.org/wiki/Snake_case)，如：view_count, is_vip
 - 数据库表主键 **必须** 为「id」
 - 数据模型变量 **必须** 为「resource_id」，如：$user_id, $post_id

 
### 利用 Trait 来扩展数据模型
 
 有时候数据模型里的代码会变得很臃肿，**应该** 利用 Trait 来精简逻辑代码量，提高可读性，类似于 [Ruby China](https://github.com/ruby-china/ruby-china/blob/master/app/models/topic.rb#L11-L17) 源码。
 
  > 借鉴于 Rails 的设计理念：「Fat Models, Skinny Controllers」。
 
 存放于文件夹：``app/Models/Traits`` 文件夹中。
## 辅助函数

### 存放位置

Laravel 提供了很多 [辅助函数](http://d.laravel-china.org/docs/5.5/helpers)，有时候我们也需要创建自己的辅助函数。

必须 把所有的『自定义辅助函数』存放于 ``app/helper/functions`` 文件中。

### 函数规范

**必须** 判断函数是否已经存在，命名尽量的具有唯一的指向性并添加注释

实现比较复杂且使用比较频繁的函数 **应该** 放在全局辅助函数中

```php
if (!function_exists('rong_cloud')) {
    /**
     * 融云sdk
     * @param $name
     * @return mixed
     */
    function rong_cloud($name)
    {
        return call_user_func([\App\Library\Core\RongCloud::class,$name]);
    }
}
```